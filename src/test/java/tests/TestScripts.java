package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.ParfurmPage;
import pojos.parfurm.FiltersParameters;
import pojos.parfurm.SearchParameters;
public class TestScripts extends TestBase {

    @Test(dataProvider = "testOne", dataProviderClass = Parameters.class)
    public void testOne(SearchParameters searchParameters) throws InterruptedException {
        HomePage homePage = PageFactory.initElements(getDriver(), HomePage.class);
        homePage.acceptAllCookies();
        ParfurmPage parfurmPage = homePage.clickOnParfurm();
        for(FiltersParameters filter : searchParameters.getFilters()){
            parfurmPage.setFilters(filter);
            parfurmPage.removeFilters();
        }
    }
}
