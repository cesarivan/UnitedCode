package tests;

import factories.DriverFactory;
import interfases.ITestBase;
import listener.ListenerExample;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

@Listeners(ListenerExample.class)
public class TestBase implements ITestBase {
    private ThreadLocal<WebDriver> threadDrivers = new ThreadLocal<>();

    @BeforeTest(alwaysRun=true)
    public void setup(){
        threadDrivers.set(DriverFactory.getDriver());
        getDriver().get(System.getProperty("url"));
    }

    @AfterTest(alwaysRun=true)
    public void tears(){
        if(getDriver() != null)
            getDriver().close();
    }

    public WebDriver getDriver(){
        return threadDrivers.get();
    }

}
