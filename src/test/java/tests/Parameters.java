package tests;

import com.google.gson.Gson;
import org.testng.annotations.DataProvider;
import pojos.parfurm.SearchParameters;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Parameters {
    @DataProvider(name = "testOne")
    public static Object[][] testOneParameters() throws IOException {
        Gson gson = new Gson();
        Reader reader = Files.newBufferedReader(Paths.get("src/test/java/parameters/parfurm/search/SearchParameters.json"));
        SearchParameters parameters = gson.fromJson(reader, SearchParameters.class);
        return new Object[][] {{parameters}};
    }
}
