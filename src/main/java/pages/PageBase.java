package pages;

import helpers.ElementHelper;
import org.openqa.selenium.WebDriver;

public class PageBase {
    protected WebDriver driver;
    protected ElementHelper elementHelper;
    public  PageBase(WebDriver driver){
        this.driver = driver;
        elementHelper = new ElementHelper(driver);
    }
}
