package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pojos.parfurm.FiltersParameters;

public class ParfurmPage extends CommonPage{
    private String elementCheckboxXpath = "//div[contains(text(),'%s')]";
    public ParfurmPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//div[text() = 'Produktart']")
    private WebElement producktartSelect;
    @FindBy(xpath = "//input[@name = 'facet-search']")
    private WebElement searchTextbox;
    @FindBy(xpath = "//div[text() = 'Marke']")
    private WebElement markeSelect;
    @FindBy(xpath = "//div[text() = 'Für Wen']")
    private WebElement furWenSelect;
    @FindBy(xpath = "//div[text() = 'Geschenk für']")
    private WebElement geschenkFurSelect;
    @FindBy(xpath = "//div[text() = 'Highlights']")
    private WebElement highlightsSelect;
    @FindBy(xpath = "//button[@class='button button__primary facet__close-button']")
    private WebElement closeSelectButton;
    @FindBy(xpath = "(//div[@class='selected-facets']//a)[1]")
    private WebElement removeAllFilterTag;
    public void setFilters(FiltersParameters filters) throws InterruptedException {
        if(filters.getHighLights() != null){
            elementHelper.click(highlightsSelect);
            //Review possible defect in the app
            Thread.sleep(1000);
            elementHelper.click(How.XPATH, String.format(elementCheckboxXpath, filters.getHighLights()));
            if(elementHelper.isPresent(closeSelectButton, 5))
                elementHelper.click(closeSelectButton);
            elementHelper.waitUntilElementIsPresent(How.XPATH, "//div[text() = 'Highlights']//*[@class='arrow-icon icon icon--SVG_19 icon--color-success']");
        }
        if(filters.getProducktart() != null){
            elementHelper.click(producktartSelect);
            elementHelper.sendKeys(searchTextbox, filters.getProducktart());
            elementHelper.click(How.XPATH, String.format(elementCheckboxXpath, filters.getProducktart()));
            if(elementHelper.isPresent(closeSelectButton, 5))
                elementHelper.click(closeSelectButton);
            elementHelper.waitUntilElementIsPresent(How.XPATH, "//div[text() = 'Produktart']//*[@class='arrow-icon icon icon--SVG_19 icon--color-success']");
        }
        if(filters.getMarke() != null){
            elementHelper.click(markeSelect);
            elementHelper.sendKeys(searchTextbox, filters.getMarke());
            elementHelper.click(How.XPATH, String.format(elementCheckboxXpath, filters.getMarke()));
            if(elementHelper.isPresent(closeSelectButton, 5))
                elementHelper.click(closeSelectButton);
            elementHelper.waitUntilElementIsPresent(How.XPATH, "//div[text() = 'Marke']//*[@class='arrow-icon icon icon--SVG_19 icon--color-success']");
        }
        if(filters.getFurWen() != null){
            elementHelper.click(furWenSelect);
            elementHelper.click(How.XPATH, String.format(elementCheckboxXpath, filters.getFurWen()));
            if(elementHelper.isPresent(closeSelectButton, 5))
                elementHelper.click(closeSelectButton);
            elementHelper.waitUntilElementIsPresent(How.XPATH, "//div[text() = 'Für Wen']//*[@class='arrow-icon icon icon--SVG_19 icon--color-success']");
        }
        if(filters.getGeschenkFur() != null){
            elementHelper.click(geschenkFurSelect);
            elementHelper.sendKeys(searchTextbox, filters.getGeschenkFur());
            elementHelper.click(How.XPATH, String.format(elementCheckboxXpath, filters.getGeschenkFur()));
            if(elementHelper.isPresent(closeSelectButton, 5))
                elementHelper.click(closeSelectButton);
            elementHelper.waitUntilElementIsPresent(How.XPATH, "//div[text() = 'Geschenk für']//*[@class='arrow-icon icon icon--SVG_19 icon--color-success']");
        }
    }
    public void removeFilters(){
        elementHelper.click(removeAllFilterTag);
        elementHelper.waitUntilElementIsNotPresent(removeAllFilterTag);
    }
}
