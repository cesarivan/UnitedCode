package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CommonPage extends PageBase{
    public CommonPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//a[text()='PARFUM']")
    private WebElement parfurmTab;
    @FindBy(xpath = "//input[@placeholder='Wonach suchst du?']")
    private WebElement searchTextbox;
    public ParfurmPage clickOnParfurm(){
        elementHelper.click(parfurmTab);
        elementHelper.moveToElement(searchTextbox);
        return PageFactory.initElements(driver, ParfurmPage.class);
    }
    public void moveToSearchText(){
        elementHelper.click(searchTextbox);
    }
}
