package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends CommonPage{
    public HomePage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//button[@class = 'button button__primary uc-list-button__accept-all']")
    private WebElement acceptCookiesButton;
    @FindBy(xpath = "//a[text()='PARFUM']")
    private WebElement parfurmTab;

    public void acceptAllCookies(){
        elementHelper.click(acceptCookiesButton);
    }
}
