package listener;

import interfases.ITestBase;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

public class ListenerExample extends TestListenerAdapter {
    @Override
    public void onTestStart(ITestResult iTestResult) {
        super.onTestStart(iTestResult);
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        super.onTestSuccess(iTestResult);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        super.onTestFailure(iTestResult);
        Object currentClass = iTestResult.getInstance();
        if (currentClass instanceof ITestBase) {
            WebDriver driver = ((ITestBase) currentClass).getDriver();
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                Path path = Paths.get(iTestResult.getTestContext().getOutputDirectory());
                String fileName = "screenshot_"+new Date().getTime()+".png";
                System.out.println("File: " + fileName);
                FileUtils.copyFile(srcFile, new File(path.getParent()+"/screenshots/"+fileName));
                String imageSource = "./screenshots/" + fileName;
                Reporter.log("<a title= \"title\" href=\"" + imageSource + "\">" +
                        "<img width=\"418\" height=\"240\" alt=\"alternativeName\" title=\"title\" src=\"" + imageSource + "\"> </a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        super.onTestSkipped(iTestResult);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        super.onTestFailedButWithinSuccessPercentage(iTestResult);
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        super.onStart(iTestContext);
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        super.onFinish(iTestContext);
    }
}
