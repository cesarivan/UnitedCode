package interfases;

import org.openqa.selenium.WebDriver;
public interface ITestBase {
    ThreadLocal<WebDriver> threadDrivers = null;
    WebDriver getDriver();
}
