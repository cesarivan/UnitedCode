package factories;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.NoSuchDriverException;

public class DriverFactory {

    public static WebDriver getDriver(){
        WebDriver driver = null;
        switch (System.getProperty("browser").toLowerCase()){
            case "chrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            default:  throw new NoSuchDriverException("You don't specify the browser to open a Driver");
        }
        driver.manage().window().maximize();
        return driver;
    }
}
