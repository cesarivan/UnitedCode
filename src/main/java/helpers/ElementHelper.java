package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.time.Duration;


public class ElementHelper {
    private WebDriver driver;
    private WebDriverWait webDriverWait;
    public ElementHelper(WebDriver driver){
        this.driver = driver;
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(60));
    }
    public void click(WebElement element){
        isReady(element);
        element.click();
        Reporter.log("<br>Click on: " + element + "</br>");
    }
    public void click(How how, String value){
        click(getElement(how, value));
    }
    public void sendKeys(WebElement element, String text){
        isReady(element);
        element.sendKeys(text);
        Reporter.log("<br>SendKeys on: " + element + "</br>");
    }
    public boolean waitUntilElementIsPresent(How how, String value){
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);
        return wait.until(ExpectedConditions.visibilityOf(getElement(how, value))).isDisplayed();
    }
    private void isReady(WebElement element){
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    public void waitUntilElementIsNotPresent(WebElement element){
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }
    public boolean isPresent(WebElement element, int timeToWait){
        try {
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeToWait));
            wait.pollingEvery(Duration.ofSeconds(1));
            wait.until(ExpectedConditions.visibilityOf(element));
            return true;
        }catch (Exception ex){
            return false;
        }
    }
    public void moveToElement(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
    }
    private WebElement getElement(How how, String value){
        switch (how){
            case XPATH: return driver.findElement(By.xpath(value));
            default: return null;
        }
    }
}
