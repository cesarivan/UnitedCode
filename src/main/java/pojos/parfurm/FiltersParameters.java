package pojos.parfurm;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FiltersParameters {
    private String highLights;
    @JsonIgnore
    private String marke;
    private String producktart;
    @JsonIgnore
    private String geschenkFur;
    private String furWen;

    public String getHighLights() {
        return highLights;
    }

    public void setHighLights(String highLights) {
        this.highLights = highLights;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public String getProducktart() {
        return producktart;
    }

    public void setProducktart(String producktart) {
        this.producktart = producktart;
    }

    public String getGeschenkFur() {
        return geschenkFur;
    }

    public void setGeschenkFur(String geschenkFur) {
        this.geschenkFur = geschenkFur;
    }

    public String getFurWen() {
        return furWen;
    }

    public void setFurWen(String furWen) {
        this.furWen = furWen;
    }
}
