package pojos.parfurm;

import java.util.List;

public class SearchParameters {
    private List<FiltersParameters> filters;

    public List<FiltersParameters> getFilters() {
        return filters;
    }

    public void setFilters(List<FiltersParameters> filters) {
        this.filters = filters;
    }
}
